package aufgabe43;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Paint;

/**
 * Created by user on 25.06.17.
 */
public class Astroid {
    /**
     *public static void createAstroids(): Funktion, die Astroid/e erstellt
     * @param anzahl Anzahl der Astroide
     * @param radius Radius des Astroid auf der X-Achse
     * @param punkte Anzahl der Punkte, mit denen der Astroid gezeichnet werden soll
     * @param rotation Grad, nach dem der Astroid rotiert werden soll
     * @param canvas Canvas, der GraphicsContext enthaelt
     */
    public static void createAstroids(Canvas canvas, int radius, int punkte, int anzahl, double rotation){
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.setLineWidth(1);

        /*
         * int schritt: Schritte, die zwischen den einzelnen Punkten gemacht werden
         * int abstand: Abstand in Pixel zwischen den einzelnen Figuren
         */
        int schritt = 360 / punkte;
        int abstand = 8;
        gc.setTransform(1.0, 0, 0, 1.0, canvas.getWidth() / 2, canvas.getHeight() / 2);

        for (int i = 0; i <= anzahl; i += 1) {
            gc.rotate(rotation);
            gc.moveTo( ( (radius - abstand * i) / 4) * ( (3 * Math.cos(Math.toRadians(0)) + Math.cos(Math.toRadians(0)))) ,  ( (radius - abstand * i) / 4) * ( (3 * Math.sin(Math.toRadians(0)) - Math.sin(Math.toRadians(0)))) );
            for (int t = 0; t <= 360; t += schritt) {
                if((radius - abstand * i) >= 0) {
                    gc.lineTo(((radius - abstand * i) / 4) * ((3 * Math.cos(Math.toRadians(t)) + Math.cos(Math.toRadians(3 * t)))), ((radius - abstand * i) / 4) * ((3 * Math.sin(Math.toRadians(t)) - Math.sin(Math.toRadians(3 * t)))));
                    gc.moveTo(((radius - abstand * i) / 4) * ((3 * Math.cos(Math.toRadians(t)) + Math.cos(Math.toRadians(3 * t)))), ((radius - abstand * i) / 4) * ((3 * Math.sin(Math.toRadians(t)) - Math.sin(Math.toRadians(3 * t)))));
                } else { return; }
            }
        }
        gc.stroke();
        return;
    }

    public static void createNorbert(Canvas canvas, int radius, int punkte, int anzahl, double rotation) {
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.setLineWidth(1);

        /*
         * int schritt: Schritte, die zwischen den einzelnen Punkten gemacht werden
         * int abstand: Abstand in Pixel zwischen den einzelnen Figuren
         */
        int schritt = 360 / punkte;
        int abstand = 8;
        gc.setTransform(1.0, 0, 0, 1.0, canvas.getWidth() / 2, canvas.getHeight() / 2);

        for (int i = 0; i <= anzahl; i += 1) {
            gc.rotate(rotation);
            gc.moveTo(((radius - abstand * i) / 4) * (3 * (Math.cos(Math.toRadians(0) + Math.cos(Math.toRadians(0))))), ((radius - abstand * i) / 4) * (3 * (Math.sin(Math.toRadians(0) - Math.sin(Math.toRadians(0))))));
            for (int t = 0; t <= 360; t += schritt) {
                if ((radius - abstand * i) >= 0) {
                    gc.lineTo(((radius - abstand * i) / 4) * (3 * (Math.cos(Math.toRadians(t) + Math.cos(Math.toRadians(3 * t))))), ((radius - abstand * i) / 4) * (3 * (Math.sin(Math.toRadians(t) - Math.sin(Math.toRadians(3 * t))))));
                    gc.moveTo(((radius - abstand * i) / 4) * (3 * (Math.cos(Math.toRadians(t) + Math.cos(Math.toRadians(3 * t))))), ((radius - abstand * i) / 4) * (3 * (Math.sin(Math.toRadians(t) - Math.sin(Math.toRadians(3 * t))))));
                } else {
                    return;
                }
            }
        }
        gc.stroke();
        return;
    }
}
