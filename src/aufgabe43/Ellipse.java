package aufgabe43;


import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;



/**
 * Created by user on 25.06.17.
 */
public class Ellipse {

    /**
     *public static void createEllipses(): Funktion, die Ellipse/n erstellt
     * @param anzahl Anzahl der Ellipsen
     * @param radius Radius der Ellipse auf der X-Achse
     * @param punkte Anzahl der Punkte, mit denen die Ellipse gezeichnet werden soll
     * @param rotation Grad, nach dem die Ellipse rotiert werden soll
     * @param ratio Verhaeltnis zwischen den zwei Radien
     * @param canvas Canvas, der GraphicsContext enthaelt
     */



    public static void createEllipses(Canvas canvas, int radius, int punkte, int anzahl, double rotation, double ratio){
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.setLineWidth(1);

        /*
         * int schritt: Schritte, die zwischen den einzelnen Punkten gemacht werden
         * int abstand: Abstand in Pixel zwischen den einzelnen Figuren
         * int radiusX: Radius der Ellipse auf der X-Achse (Breite)
         * int radiusY: Radius der Ellipse auf der Y-Achse (Höhe)
         */
        int schritt = 360 / punkte;
        int abstand = 8;
        int radiusX = radius;
        int radiusY = (int) (radius * ratio);
        gc.setTransform(1.0, 0, 0, 1.0, canvas.getWidth() / 2, canvas.getHeight() / 2);

        for (int i = 0; i < anzahl; i += 1) {
            gc.rotate(rotation);
            gc.moveTo((Math.cos(Math.toRadians(((double) 0)))) * (radiusX - abstand * i), (Math.sin(Math.toRadians(((double) 0)))) * ( (radiusY - abstand * i) ));
            for (int t = 0; t <= 360; t += schritt) {
                if((radiusX - abstand * i) >= 0 && (radiusY - abstand * i) >= 0) {
                    gc.lineTo((Math.cos(Math.toRadians(((double) t)))) * (radiusX - abstand * i), (Math.sin(Math.toRadians(((double) t)))) * ( (radiusY - abstand * i) ));
                    gc.moveTo((Math.cos(Math.toRadians(((double) t)))) * (radiusX - abstand * i), (Math.sin(Math.toRadians(((double) t)))) * ( (radiusY - abstand * i)  ));
                } else { return; }
            }
        }
        gc.stroke();
        return;
    }
}
