package aufgabe43;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;

/**
 * Created by user on 25.06.17.
 */
public class Circle {
    /**
     *public static void createCircles(): Funktion, die Kreis/e erstellt
     * @param anzahl Anzahl der Kreise
     * @param radius Radius des Kreises auf der X-Achse
     * @param punkte Anzahl der Punkte, mit denen der Kreis gezeichnet werden soll
     * @param rotation Grad, nach dem der Kreis rotiert werden soll
     * @param canvas Canvas, der GraphicsContext enthaelt
     */
    public static void createCircles(Canvas canvas, int radius, int punkte, int anzahl, double rotation){
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.setLineWidth(1);

        /*
         * int schritt: Schritte, die zwischen den einzelnen Punkten gemacht werden
         * int abstand: Abstand in Pixel zwischen den einzelnen Figuren
         */
        int schritt = 360 / punkte;
        int abstand = 10;
        gc.setTransform(1.0, 0, 0, 1.0, canvas.getWidth() / 2, canvas.getHeight() / 2);

        for (int i = 0; i < anzahl; i += 1) {
            gc.rotate(rotation);
            gc.moveTo( (Math.cos(Math.toRadians(((double) 0)))) * (radius - abstand * i), (Math.sin(Math.toRadians(((double) 0)))) * (radius - abstand * i));
            for (int t = 0; t <= 360; t += schritt) {
                if((radius - abstand * i) >=0) {
                    gc.lineTo((Math.cos(Math.toRadians(((double) t)))) * (radius - abstand * i), (Math.sin(Math.toRadians(((double) t)))) * (radius - abstand * i));
                    gc.moveTo((Math.cos(Math.toRadians(((double) t)))) * (radius - abstand * i), (Math.sin(Math.toRadians(((double) t)))) * (radius - abstand * i));
                } else { return; }
            }
        }
        gc.stroke();
        return;
    }
}
