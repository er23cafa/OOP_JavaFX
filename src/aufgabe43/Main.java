package aufgabe43;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class Main extends Application {
    /**
     * Main Klasse mit entry point durch eine start() Methode
     * @param primaryStage Die aktuelle Stage
     * @throws IOException
     */
    @Override
    public void start(Stage primaryStage) throws IOException{
        Parent root = null;
        try{
            root = FXMLLoader.load(getClass().getResource("UI.fxml"));
        } catch (IOException e){
            e.printStackTrace();
        }
        primaryStage.setTitle("Kurven");
        primaryStage.setScene(new Scene(root, 800, 800));
        primaryStage.show();
    }

    public static void main(String[] args) { launch(args);}
}
