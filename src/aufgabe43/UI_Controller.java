package aufgabe43;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class UI_Controller implements  Initializable{
    @FXML public SplitPane rootPane;
    @FXML public AnchorPane topPane;
    @FXML public VBox vBox;
    @FXML public Canvas canvasPane;
    @FXML public AnchorPane bottomPane;
    public ColorPicker colorPicker;
    @FXML public ChoiceBox choices;
    public Button createButton;
    public Button closeButton;
    public Slider radiusSlider;
    public Slider anzahlSlider;
    public Slider rotationSlider;
    public Slider punkteSlider;
    public Slider ratioSlider;
    public Label ratioLabel;
    public Label radiusDisplay;
    public Label punkteDisplay;
    public Label anzahlDisplay;
    public Label rotationDisplay;
    public Label ratioDisplay;

    /**
     * Called to initialize a controller after its root element has been
     * completely processed.
     *
     * @param location  The location used to resolve relative paths for the root object, or
     *                  <tt>null</tt> if the location is not known.
     * @param resources The resources used to localize the root object, or <tt>null</tt> if
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        choices.setItems(FXCollections.observableArrayList("Kreis", "Ellipse", "Asteroid", "Norbert"));
        ratioDisplay.setVisible(false);
        ratioSlider.setVisible(false);
        ratioLabel.setVisible(false);
        choices.setValue("Kreis");
        colorPicker.setValue(Color.BLACK);
    }

    /**
     * Erschafft einen neuen Canvas und Zeichnet nach den im GUI eingestellten Parametern Figuren.
     */
    public void create(){
        Stage appStage = (Stage) rootPane.getScene().getWindow();
        vBox.getChildren().remove(0);
        canvasPane = new Canvas(800, 600);
        vBox.getChildren().add(canvasPane);
        canvasPane.getGraphicsContext2D().setStroke(colorPicker.getValue());

        switch ((String) choices.getValue()){
            case "Kreis":
                Circle.createCircles(canvasPane, (int) radiusSlider.getValue(), (int) punkteSlider.getValue(), (int) anzahlSlider.getValue(), rotationSlider.getValue());
                appStage.show();
                break;
            case "Ellipse":
                Ellipse.createEllipses(canvasPane, (int) radiusSlider.getValue(), (int) punkteSlider.getValue(), (int) anzahlSlider.getValue(), rotationSlider.getValue(), ratioSlider.getValue());

                appStage.show();
            case "Asteroid":
                Astroid.createAstroids(canvasPane, (int) radiusSlider.getValue(), (int) punkteSlider.getValue(), (int) anzahlSlider.getValue(), rotationSlider.getValue());

                appStage.show();
            case "Norbert":
                Astroid.createNorbert(canvasPane, (int) radiusSlider.getValue(), (int) punkteSlider.getValue(), (int) anzahlSlider.getValue(), rotationSlider.getValue());

                appStage.show();
        }
    }

    /**
     * Falls "Ellipse" im DropDown Menue ausgewählt wurde werden die Kruemmungseinstellungen angezeigt.
     * @param mouseEvent tritt auf im Fall "OnMouseExited"
     */
    public void showRatio(MouseEvent mouseEvent) {
        if (choices.getValue().equals("Ellipse")){
            ratioLabel.setVisible(true);
            ratioSlider.setVisible(true);
            ratioDisplay.setVisible(true);
        } else {
            ratioLabel.setVisible(false);
            ratioSlider.setVisible(false);
            ratioDisplay.setVisible(false);
        }
    }

    public void close(){
        System.exit(0);
    }
}
